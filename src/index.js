import React from 'react'
import { render } from 'react-dom'
import './index.css'
import * as serviceWorker from './serviceWorker'

import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import Reducers from './reducers'
import Routes from './Routes'

const store = createStore(Reducers, applyMiddleware(thunk))

render(<Routes store={store} />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
