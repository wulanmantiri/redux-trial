import { ADD_PRODUCT, CANCEL_PRODUCT, INITIALIZE_PRODUCT, PURCHASE } from '../actions'

const EMPTY_CART = {
  products: [],
  total: 0,
  price: 0,
  hasItems: false,
}

const cartReducer = (state = EMPTY_CART, action) => {
  switch (action.type) {
    case INITIALIZE_PRODUCT:
      return {
        ...state,
        products: [
          ...state.products,
          {
            id: action.id,
            title: action.title,
            price: action.price,
            selected: false,
          },
        ],
      }
    case ADD_PRODUCT: {
      const products = state.products.map(product =>
        product.id === action.id ? { ...product, selected: true } : product,
      )
      const price = state.price + action.price
      const total = state.total + 1
      return {
        ...state,
        products,
        total,
        price: Number(price.toFixed(2)),
        hasItems: true,
      }
    }
    case CANCEL_PRODUCT:
      const products = state.products.map(product =>
        product.id === action.id ? { ...product, selected: false } : product,
      )
      const price = state.price - action.price
      const total = state.total - 1
      let hasItems = true
      if (total === 0) hasItems = false
      return {
        ...state,
        products,
        total,
        price: Number(price.toFixed(2)),
        hasItems,
      }
    case PURCHASE:
      return EMPTY_CART
    default:
      return state
  }
}

export default cartReducer
