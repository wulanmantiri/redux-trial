import { combineReducers } from 'redux'
import cartReducer from './cartReducer'
import inputReducer from './inputReducer'

export default combineReducers({
  cartReducer,
  inputReducer,
})
