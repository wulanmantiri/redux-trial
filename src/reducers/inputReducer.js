import { InputTypes, CouponCodes, PURCHASE } from '../actions'

const INITIAL_INPUTS = {
  discountedPrice: 0,
  couponCode: '',
  address: '',
  deliveryProvider: '',
  paid: false,
}

const giveDiscount = (code, price) => {
  switch (code) {
    case CouponCodes.FIFTY_PERCENT:
      return price * 0.5
    case CouponCodes.THIRTY_PERCENT:
      return price * 0.7
    case CouponCodes.TWENTY_PERCENT:
      return price * 0.8
    default:
      return price
  }
}

const inputReducer = (state = INITIAL_INPUTS, action) => {
  switch (action.type) {
    case InputTypes.ADDRESS:
      return {
        ...state,
        address: action.value,
      }
    case InputTypes.COUPON:
      return {
        ...state,
        couponCode: action.value,
        discountedPrice: giveDiscount(action.value, action.price),
      }
    case InputTypes.PROVIDER:
      return {
        ...state,
        deliveryProvider: action.value,
      }
    case InputTypes.PAYMENT:
      return {
        ...state,
        paid: true,
      }
    case InputTypes.RESET_COUPON:
      return {
        ...state,
        paid: false,
        discountedPrice: 0,
      }
    case PURCHASE:
      return INITIAL_INPUTS
    default:
      return state
  }
}

export default inputReducer
