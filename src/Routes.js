import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { Provider } from 'react-redux'
import Navbar from './components/Navbar'
import PageProductList from './containers/PageProductList'
import PageCheckout from './containers/PageCheckout'
import PageDelivery from './containers/PageDelivery'

const Routes = ({ store }) => (
  <Provider store={store}>
    <BrowserRouter>
      <Navbar />
      <Switch>
        <Route exact path="/" component={PageProductList} />
        <Route path="/checkout" component={PageCheckout} />
        <Route path="/delivery" component={PageDelivery} />
      </Switch>
    </BrowserRouter>
  </Provider>
)

export default Routes
