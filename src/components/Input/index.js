import React from 'react'
import styled from 'styled-components'

const StyledInput = styled.input`
  border: 0.05rem solid #808080;
  border-radius: 0.5rem;
  width: 15rem;
  height: 2rem;
  padding: 0.2rem;
`

const StyledLabel = styled.p`
  font-weight: bold;
  margin: 0;
`

const Input = props => (
  <div style={{ marginBottom: '3vh' }}>
    <StyledLabel>{props.label}</StyledLabel>
    <StyledInput
      type={props.type}
      name={props.name}
      value={props.value}
      onChange={props.onChange}
      placeholder={props.placeholder}
    />
  </div>
)

export default Input
