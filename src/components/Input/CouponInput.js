import React from 'react'
import styled from 'styled-components'
import Button from '../Button'

const StyledInput = styled.input`
  border: 0.05rem solid #808080;
  border-radius: 0.3rem;
  width: 10rem;
  height: 2rem;
  padding: 0.2rem;
  margin-right: 0.5vw;
`

const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  @media (max-width: 800px) {
    flex-direction: column;
    align-items: flex-start;
  }
`

export const CouponInput = props => (
  <Container>
    <StyledInput
      type="text"
      name="coupon"
      value={props.value}
      onChange={props.onChange}
      placeholder="Insert coupon code"
    />
    <Button onClick={props.onButtonClick} bgcolor="#00B300" width="4rem" content="Submit" />
  </Container>
)
