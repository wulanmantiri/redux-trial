import React from 'react'
import styled from 'styled-components'

const StyledButton = styled.button`
  background-color: #fff;
  border-radius: 0.3rem;
  border: 0.05rem solid ${props => props.bgcolor};
  height: 2rem;
  width: ${props => props.width};
  padding: 0.2rem;
  cursor: pointer;

  font-family: 'Harmattan', 'Segoe UI', sans-serif;
  font-size: 1.1rem;
  text-align: center;
  color: #000;

  &:hover {
    background-color: ${props => props.bgcolor};
    color: #fff;
  }

  &:disabled {
    background-color: #808080;
    color: #fff;
    border: 0;
    cursor: initial;
  }
`

const Button = props => (
  <StyledButton
    onClick={props.onClick}
    bgcolor={props.bgcolor}
    width={props.width}
    disabled={props.disabled}
  >
    {props.content}
  </StyledButton>
)

export default Button
