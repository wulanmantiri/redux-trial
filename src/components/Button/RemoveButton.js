import React from 'react'
import Button from '.'

export const RemoveButton = props => (
  <Button onClick={props.onClick} bgcolor="#E24C00" width="4rem" content="Remove" />
)
