import React from 'react'
import Button from '.'

export const BuyButton = props => (
  <Button
    onClick={props.onClick}
    bgcolor="#00B300"
    width="4rem"
    content="Buy"
    disabled={props.disabled}
  />
)
