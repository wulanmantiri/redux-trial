import React from 'react'
import Button from '.'

export const WideButton = props => (
  <Button onClick={props.onClick} bgcolor="#8a2be2" width="6rem" content={props.content} />
)
