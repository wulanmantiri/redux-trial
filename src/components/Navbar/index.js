import React from 'react'
import styled from 'styled-components'
import { Link, NavLink } from 'react-router-dom'

const NavContainer = styled.div`
  max-width: 100vw;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 1em 5vw;
  font-size: 1.3rem;

  @media (max-width: 500px) {
    flex-direction: column;
    align-items: flex-start;
    padding: 2em 7vw 0;
  }
`

const NavLogo = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;

  @media (max-width: 500px) {
    margin-bottom: 0.75rem;
  }
`

const NavList = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-right: 1.5vw;
  @media (max-width: 500px) {
    flex-direction: column;
    align-items: flex-start;
  }
`

const NavItem = styled(NavLink)`
  margin: 0 1rem;
  text-decoration: none;
  cursor: pointer;
  color: #808080;
  align-items: center;
  display: flex;
  flex-direction: column;

  &:before {
    background-color: #000;
    height: 1px;
    width: 0%;
    transition: all 0.25s ease-in-out;
    margin-bottom: 0.1rem;
    content: '';
  }

  &:hover:before {
    width: 100%;
  }

  &.active {
    color: #000;
  }

  @media (max-width: 500px) {
    margin: 0.15rem 0;
  }
`

const Navbar = () => (
  <NavContainer>
    <Link to="/" style={{ textDecoration: 'none' }}>
      <NavLogo>E-Commerce</NavLogo>
    </Link>
    <NavList>
      <NavItem exact to="/">
        Home
      </NavItem>
      <NavItem to="/checkout">Checkout</NavItem>
      <NavItem to="/delivery">Delivery</NavItem>
    </NavList>
  </NavContainer>
)

export default Navbar
