import React from 'react'
import styled from 'styled-components'
import { BuyButton } from '../../components/Button/BuyButton'
import { RemoveButton } from '../../components/Button/RemoveButton'

import { connect } from 'react-redux'
import { addProduct, cancelProduct, setInput, InputTypes } from '../../actions'

const Container = styled.tr`
  padding: 0.5rem;
  width: 100%;
  height: 8vh;
`

const LargeColumn = styled.td`
  width: 70%;
  border-bottom: 0.05rem solid #c5c5c5;
`

const SmallColumn = styled.td`
  width: 15%;
  border-bottom: 0.05rem solid #c5c5c5;
`

const Product = props => (
  <Container>
    <LargeColumn>{props.title}</LargeColumn>
    <SmallColumn>{props.price}</SmallColumn>
    <SmallColumn>
      {props.button === 'Buy' ? (
        <BuyButton
          onClick={() => props.addProduct(props.id, props.price)}
          disabled={props.selected}
        />
      ) : (
        <RemoveButton onClick={() => props.cancelProduct(props.id, props.price)} />
      )}
    </SmallColumn>
  </Container>
)

const mapDispatchToProps = dispatch => ({
  addProduct: (id, price) => {
    dispatch(addProduct(id, price))
    dispatch(setInput(InputTypes.RESET_COUPON, {}))
  },
  cancelProduct: (id, price) => {
    dispatch(cancelProduct(id, price))
    dispatch(setInput(InputTypes.RESET_COUPON, {}))
  },
})

export default connect(null, mapDispatchToProps)(Product)
