import React from 'react'
import styled from 'styled-components'
import { Link, Redirect } from 'react-router-dom'

import { connect } from 'react-redux'
import { setInput, InputTypes, VisibilityFilters } from '../../actions'
import ProductsList from '../ProductsList'
import { WideButton } from '../../components/Button/WideButton'
import { CouponInput } from '../../components/Input/CouponInput'

const Layout = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

const Row = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 45vw;
  margin: 5vh 0 0;
`

const LeftAlignedDiv = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`

const RightAlignedDiv = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  line-height: 1rem;
`

const CouponDiv = styled.div`
  margin-top: 2vh;
  min-height: 4rem;
`

const PageCheckout = props => {
  let couponResponseText
  if (props.couponCode === '') {
    couponResponseText = <p>Please fill the coupon code.</p>
  } else if (props.discountedPrice === props.price) {
    couponResponseText = <p>Sorry, your coupon code is invalid.</p>
  } else {
    couponResponseText = <p>Discount! Now, you only have to pay {props.discountedPrice}</p>
  }

  if (!props.hasItems) {
    return <Redirect to="/" />
  }
  return (
    <Layout>
      <Row>
        <LeftAlignedDiv>
          <h1>Invoice</h1>
        </LeftAlignedDiv>
        <RightAlignedDiv>
          <p>Qty: {props.total}</p>
          <p>Price: {props.price}</p>
        </RightAlignedDiv>
      </Row>
      <ProductsList button="Remove" filter={VisibilityFilters.SELECTED} />
      <Row>
        <CouponInput
          value={props.couponCode}
          onChange={e => props.setCouponCode(e.target.value, 0)}
          onButtonClick={() => props.setCouponCode(props.couponCode, props.price)}
        />
        <RightAlignedDiv>
          <Link to="/delivery">
            <WideButton
              content="Pay"
              onClick={() => {
                props.setCouponCode(props.couponCode, props.price)
                props.pay()
              }}
            />
          </Link>
        </RightAlignedDiv>
      </Row>
      {props.discountedPrice !== 0 && <CouponDiv>{couponResponseText}</CouponDiv>}
    </Layout>
  )
}

const mapStateToProps = state => ({
  total: state.cartReducer.total,
  price: state.cartReducer.price,
  hasItems: state.cartReducer.hasItems,
  discountedPrice: state.inputReducer.discountedPrice,
  couponCode: state.inputReducer.couponCode,
})

const mapDispatchToProps = dispatch => ({
  setCouponCode: (value, price) => dispatch(setInput(InputTypes.COUPON, { value, price })),
  pay: () => dispatch(setInput(InputTypes.PAYMENT, {})),
})

export default connect(mapStateToProps, mapDispatchToProps)(PageCheckout)
