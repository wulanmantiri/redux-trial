import React, { useEffect } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import { connect } from 'react-redux'
import { initializeProduct, VisibilityFilters } from '../../actions'

import data from '../../datadummy'
import ProductsList from '../ProductsList'
import { WideButton } from '../../components/Button/WideButton'

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;
`

const RightAlignedDiv = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  margin-top: 8vh;
`

const StyledText = styled.p`
  width: 45vw;
  text-align: center;
`

const PageProductList = ({ total, initialize }) => {
  useEffect(() => {
    data.forEach(product => initialize(product))
  })

  return (
    <Container>
      <div>
        <h1>Products</h1>
        <ProductsList button="Buy" />
      </div>
      <div>
        <h1>Shopping Cart</h1>
        {total === 0 ? (
          <StyledText>No products</StyledText>
        ) : (
          <div>
            <ProductsList button="Remove" filter={VisibilityFilters.SELECTED} />
            <RightAlignedDiv>
              <Link to="/checkout">
                <WideButton content="Checkout" />
              </Link>
            </RightAlignedDiv>
          </div>
        )}
      </div>
    </Container>
  )
}

const mapStateToProps = state => ({
  total: state.cartReducer.total,
})

const mapDispatchToProps = dispatch => ({
  initialize: product => dispatch(initializeProduct(product)),
})

export default connect(mapStateToProps, mapDispatchToProps)(PageProductList)
