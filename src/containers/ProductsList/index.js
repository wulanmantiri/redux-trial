import React from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { VisibilityFilters } from '../../actions'
import Product from '../Product'

const StyledTable = styled.table`
  width: 45vw;
`

const ProductsList = ({ button, products }) => (
  <StyledTable>
    <tbody>
      {products.map(product => {
        return <Product key={product.id} {...product} button={button} />
      })}
    </tbody>
  </StyledTable>
)

const getProducts = (products, filter) => {
  switch (filter) {
    case VisibilityFilters.SELECTED:
      return products.filter(p => p.selected)
    case VisibilityFilters.ALL:
    default:
      return products
  }
}

const mapStateToProps = (state, ownProps) => ({
  products: getProducts(state.cartReducer.products, ownProps.filter),
})

export default connect(mapStateToProps)(ProductsList)
