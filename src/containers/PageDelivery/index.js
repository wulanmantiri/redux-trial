import React from 'react'
import styled from 'styled-components'
import { Redirect } from 'react-router-dom'

import { connect } from 'react-redux'
import { setInput, InputTypes, purchasePost } from '../../actions'

import { WideButton } from '../../components/Button/WideButton'
import Input from '../../components/Input'

const Layout = styled.div`
  min-height: 70vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

const PageDelivery = props => {
  if (!props.hasItems) {
    return <Redirect to="/" />
  } else if (!props.paid) {
    return <Redirect to="/checkout" />
  }

  return (
    <Layout>
      <h1>Delivery</h1>
      <Input
        label="Address"
        type="text"
        name="address"
        value={props.address}
        onChange={e => props.setAddress(e.target.value)}
        placeholder="Jl. Kutek Universitas Indonesia"
      />
      <Input
        label="Delivery Provider"
        type="text"
        name="provider"
        value={props.provider}
        onChange={e => props.setProvider(e.target.value)}
        placeholder="JNE Express"
      />
      <p>
        Purchase: {props.total} item(s), Cost: {props.discountedPrice}
      </p>
      <WideButton content="Purchase" onClick={() => props.purchase(props.inputs)} />
    </Layout>
  )
}

const mapStateToProps = state => ({
  inputs: {
    ...state.cartReducer,
    ...state.inputReducer,
  },
  total: state.cartReducer.total,
  hasItems: state.cartReducer.hasItems,
  discountedPrice: state.inputReducer.discountedPrice,
  address: state.inputReducer.address,
  provider: state.inputReducer.deliveryProvider,
  paid: state.inputReducer.paid,
})

const mapDispatchToProps = dispatch => ({
  setAddress: value => dispatch(setInput(InputTypes.ADDRESS, { value })),
  setProvider: value => dispatch(setInput(InputTypes.PROVIDER, { value })),
  purchase: inputs => dispatch(purchasePost(inputs)),
})

export default connect(mapStateToProps, mapDispatchToProps)(PageDelivery)
