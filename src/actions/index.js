let currentId = 0
export const initializeProduct = product => ({
  ...product,
  type: INITIALIZE_PRODUCT,
  id: currentId++,
})

export const addProduct = (id, price) => ({
  type: ADD_PRODUCT,
  id,
  price,
})

export const cancelProduct = (id, price) => ({
  type: CANCEL_PRODUCT,
  id,
  price,
})

export const setInput = (type, content) => ({
  type,
  ...content,
})

export const purchase = (status, inputs) => ({
  type: PURCHASE,
  status,
  ...inputs,
})

export const recordError = error => ({
  type: RECORD_ERROR,
  error,
})

export const purchasePost = inputs => {
  return dispatch => {
    fetch('https://httpbin.org/post', {
      method: 'post',
      body: JSON.stringify(inputs),
    })
      .then(response => dispatch(purchase(response.status, inputs)))
      .catch(error => {
        dispatch(recordError(error))
        console.log(error)
      })
  }
}

export const INITIALIZE_PRODUCT = 'INITIALIZE_PRODUCT'
export const ADD_PRODUCT = 'ADD_PRODUCT'
export const CANCEL_PRODUCT = 'CANCEL_PRODUCT'
export const PURCHASE = 'PURCHASE'
export const RECORD_ERROR = 'RECORD_ERROR'

export const VisibilityFilters = {
  ALL: 'ALL',
  SELECTED: 'SELECTED',
}

export const InputTypes = {
  COUPON: 'COUPON_CODE',
  ADDRESS: 'ADDRESS',
  PROVIDER: 'DELIVERY_PROVIDER',
  PAYMENT: 'PAYMENT',
  RESET_COUPON: 'RESET_COUPON',
}

export const CouponCodes = {
  FIFTY_PERCENT: 'WULAN',
  THIRTY_PERCENT: 'PTI',
  TWENTY_PERCENT: 'JODEPGETS',
}
